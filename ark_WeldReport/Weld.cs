﻿// Type: AutoWelding.Weld
// Assembly: ModelWelding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6CE0C539-AA48-4BBF-A64D-F4872B143C5E
// Assembly location: C:\Users\Sergey_2\AppData\Roaming\Skype\My Skype Received Files\ModelWelding.exe

using System;
using System.Collections.Generic;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;
using Tekla.Structures.Model.UI;
using Tekla.Structures.Solid;

namespace AutoWelding
{
    public static class Weld
    {
        public static List<double> Get(ModelObject object1, ModelObject object2, double maxGap = 5, double minEdgeLength = 10, double tolerance = 10, bool isShopWeld = true, bool isKatetByValue = false, double katetValue = 0, bool isFirstObjMainPart = false)
        {
            Solid solid1 = (object2 as Part).GetSolid();
            if (solid1 == null)
                return null;
            EdgeEnumerator edgeEnumerator = solid1.GetEdgeEnumerator();
            while (edgeEnumerator.MoveNext())
            {
                Edge edge = edgeEnumerator.Current as Edge;
                if (edge != null && Distance.PointToPoint(edge.StartPoint, edge.EndPoint) >= minEdgeLength)
                {
                    Solid solid2 = (object1 as Part).GetSolid();
                    if (solid2 != null)
                    {
                        FaceEnumerator faceEnumerator = solid2.GetFaceEnumerator();
                        while (faceEnumerator.MoveNext())
                        {
                            Face current = faceEnumerator.Current as Face;
                            if (current != null)
                            {
                                Point Origin = new Point();
                                LoopEnumerator loopEnumerator = current.GetLoopEnumerator();
                                int num1 = 0;
                                int num2 = 0;
                                while (loopEnumerator.MoveNext())
                                {
                                    if (num1 <= 1)
                                    {
                                        VertexEnumerator vertexEnumerator = (loopEnumerator.Current as Loop).GetVertexEnumerator();
                                        while (vertexEnumerator.MoveNext())
                                        {
                                            if (num2 <= 1)
                                                Origin = vertexEnumerator.Current as Point;
                                            ++num2;
                                        }
                                    }
                                    ++num1;
                                }
                                Vector normal = current.Normal;
                                Vector vector = new Vector(edge.EndPoint.X - edge.StartPoint.X, edge.EndPoint.Y - edge.StartPoint.Y, edge.EndPoint.Z - edge.StartPoint.Z);
                                GeometricPlane Plane = new GeometricPlane(Origin, normal);
                                if (vector.GetAngleBetween(normal) > Math.PI / 3.0)
                                {
                                    Point point1 = Projection.PointToPlane(edge.StartPoint, Plane);
                                    Point Point2 = Projection.PointToPlane(edge.EndPoint, Plane);

                                    WeldItem weldItem = new WeldItem(object1, object2, edge, current, new LineSegment(point1, Point2), maxGap, tolerance, isKatetByValue, katetValue);
                                    return weldItem.GetWeldSize();
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}
