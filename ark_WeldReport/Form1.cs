﻿using System;
using System.Windows.Forms;

using Tekla.Structures.Model;
using ui = Tekla.Structures.Model.UI;

using excel = OfficeOpenXml;
using System.IO;
using System.Collections.Generic;

using System.Linq;

namespace ark_WeldReport
{
    public class Form1 : Form
    {
        private Label lbl_NumberProject = new Label();
        private TextBox txt_NumberProject = new TextBox();
        private Button btn_Work = new Button();
        //private TextBox txt_
        public Form1()
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Width = 200;
            this.Height = 100;

            lbl_NumberProject.AutoSize = false;
            lbl_NumberProject.Text = "Номер проекта: ";
            lbl_NumberProject.Width = 75;
            lbl_NumberProject.Top = 10;
            lbl_NumberProject.Left = 10;

            txt_NumberProject.Top = 10;
            txt_NumberProject.Text = "";
            txt_NumberProject.Left = 90;
            txt_NumberProject.Width = 100;

            btn_Work.Width = 100;
            btn_Work.Left = (int)(this.Width * 0.5 - 50);
            btn_Work.Top = 50;
            btn_Work.Text = "Сформировать";
            btn_Work.Click += Btn_Work_Click;

            this.Controls.Add(lbl_NumberProject);
            this.Controls.Add(txt_NumberProject);
            this.Controls.Add(btn_Work);

        }

        string letter(int i)
        {
            string text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return text.Substring(i, 1);
        }


        private Dictionary<Assembly, int> GetObjects()
        {
            Dictionary<Assembly, int> dic = new Dictionary<Assembly, int>();
            Model model = new Model();
            if (model.GetConnectionStatus() == true)
            {
                ModelObjectEnumerator objects = new ui.ModelObjectSelector().GetSelectedObjects();
                while (objects.MoveNext())
                {
                    if (objects.Current is Assembly)
                    {
						Assembly asm = objects.Current as Assembly;
						string prefix = ""; asm.GetReportProperty("ASSEMBLY_POS", ref prefix);
                        bool flag = false; int number = 0; int iter = 0;
                        foreach(KeyValuePair<Assembly, int> k in dic)
                        {
                            string iprefix = ""; k.Key.GetReportProperty("ASSEMBLY_POS", ref iprefix);
                            if (iprefix == prefix) {
                                flag = true;
                                number = iter;
                                break;
                            }
                            iter++;
                        }

                        if (flag)
                        {
                            dic[dic.ElementAt(number).Key]++;
                        }
                        else 
                        {
                            dic.Add(asm, 1);
                        }
                    }
                }
            }
            return dic;
        }

        private class ark_weld
        {
            public string num = ""; // номер нумерации
            public string thick1 = ""; // толщина 1
            public string thick2 = ""; // толщина 2
            public string type = ""; // стыковой или угловой
            public double above = 0; //катет
            public double wpsnr = 135;
            public string material = "";
            public double length = 0.0;
        }

        /// <summary>
        ///  номер " катет, толщина, толщина " 
        /// </summary>
        /// <returns>The welds.</returns>
        /// <param name="asm">Asm.</param>
        private List<ark_weld> GetWelds(Assembly asm)
        {
            List<ark_weld> w = new List<ark_weld>();
            List<int> ids = new List<int>();

            ModelObjectEnumerator wsmp = (asm.GetMainPart() as Part).GetWelds();

            List<BaseWeld> all_welds = new List<BaseWeld>();

			while (wsmp.MoveNext())
			{
				BaseWeld bw = wsmp.Current as BaseWeld;
				if (!ids.Contains(bw.Identifier.ID) && bw.ShopWeld == true)
                    all_welds.Add(bw);
			}

            foreach(Part mo in asm.GetSecondaries())
            {
				ModelObjectEnumerator wsmp0 = (mo).GetWelds();
                while (wsmp0.MoveNext())
                {
                    BaseWeld bw = wsmp0.Current as BaseWeld;
                    if (!ids.Contains(bw.Identifier.ID) && bw.ShopWeld == true)
                        all_welds.Add(bw);
                }

            }

            foreach(BaseWeld bw in all_welds)
            {
                GetWeldsEnum(bw, ref w, ref ids);
            }

            return w;
        }


        void GetWeldsEnum(BaseWeld bw, ref List<ark_weld> w, ref List<int> ids)
        {
				if (!ids.Contains(bw.Identifier.ID) && bw.ShopWeld == true)
				{
					string number = bw.ReferenceText;
					Part mobj1 = bw.MainObject as Part;
					Part mobj2 = bw.SecondaryObject as Part;
					//List<double> th = AutoWelding.Weld.Get(mobj1, mobj2);
					ark_weld aw = new ark_weld();
					aw.type = "BW";
					if (bw is PolygonWeld)
					{
						double length = 0.0;
						for (int i = 0; i < (bw as PolygonWeld).Polygon.Points.Count - 1; i++)//Tekla.Structures.Geometry3d.Point p in (bw as PolygonWeld).Polygon.Points)
						{
							Tekla.Structures.Geometry3d.Point p1 = (bw as PolygonWeld).Polygon.Points[i] as Tekla.Structures.Geometry3d.Point;
							Tekla.Structures.Geometry3d.Point p2 = (bw as PolygonWeld).Polygon.Points[i + 1] as Tekla.Structures.Geometry3d.Point;

							length += Math.Round(Tekla.Structures.Geometry3d.Distance.PointToPoint(p1, p2), 0);
						}

						aw.length = length;
					}
					if (bw.TypeAbove == BaseWeld.WeldTypeEnum.WELD_TYPE_FILLET)
					{
						aw.type = "FW";
					}
					aw.num = number;
					aw.material = mobj1.Material.MaterialString + @" / " + mobj2.Material.MaterialString;
					//aw.thick1 = th[0];
					//aw.thick2 = th[1];

					aw.thick1 = help.x_Profile.GetFlangeAndWeb(mobj1, help.x_Profile.GetProfileType(mobj1));
					aw.thick2 = help.x_Profile.GetFlangeAndWeb(mobj2, help.x_Profile.GetProfileType(mobj2));

					aw.above = bw.SizeAbove < 0.001 ? bw.SizeBelow : bw.SizeAbove;
					double wpsnr = 0; bw.GetUserProperty("Comment", ref wpsnr);
					if (wpsnr > 0.001) aw.wpsnr = wpsnr;
					ids.Add(bw.Identifier.ID);
					w.Add(aw);
					//MessageBox.Show("th 1:" + aw.thick1 + "\nth2 :" + aw.thick2);
				}
        }


        void Btn_Work_Click(object sender, EventArgs e)
        {
            string num_project = txt_NumberProject.Text;

            FileInfo nFile = new FileInfo("table.xlsx");
			
			if (nFile.Exists)
			{
				nFile.Delete();
				nFile = new FileInfo("table.xlsx");
			}
            using (excel.ExcelPackage package = new excel.ExcelPackage(nFile))
            {
                Dictionary<Assembly, int> obj = GetObjects();
                Console.WriteLine(obj.Count.ToString());
                foreach(KeyValuePair<Assembly, int> k in obj)
                {
                    string name = ""; k.Key.GetReportProperty("ASSEMBLY_POS", ref name);
                    int total = 0; k.Key.GetReportProperty("MODEL_TOTAL", ref total);
                    for (int i = 1; i <= k.Value; i++)
                    {
                        List<ark_weld> aw = GetWelds(k.Key);
                        excel.ExcelWorksheet w = package.Workbook.Worksheets.Add(name + $"({i})");
						// row
						w.Cells["A1:B1"].Value = "F66-1"; w.Cells["A1:B1"].Merge = true;
						w.Cells["C1:O1"].Value =
							$"Welding report Nr: {num_project}/Beam {name.Replace("-", "/").Replace(" ", "")} ({i}) total {total}";
						w.Cells["C1:O1"].Merge = true;
						w.Cells["P1:Q1"].Value = "Page Nr: 1"; w.Cells["P1:Q1"].Merge = true;
						// row
						w.Cells["A2:C2"].Value = $"SIA Engineering Steel"; w.Cells["A2:C2"].Merge = true;
						w.Cells["D2:L2"].Value = $"Contract title: NAU (MTH)"; w.Cells["D2:L2"].Merge = true;
						w.Cells["M2:Q2"].Value = "Method of counting: Drawing"; w.Cells["M2:Q2"].Merge = true;
						// row
						w.Cells["A3:E3"].Value = $"Contract Nr."; w.Cells["A3:C3"].Merge = true;
						w.Cells["D3:H3"].Value = $"Customer: MTH"; w.Cells["D3:H3"].Merge = true;
						w.Cells["I3:L3"].Value = $"Drawing Nr:    {name.Replace("-", "/").Replace(" ", "")}"; w.Cells["I3:L3"].Merge = true;
						w.Cells["M3:Q3"].Value = $"Component Nr:  {name.Replace("-", "/").Replace(" ", "")}"; w.Cells["M3:Q3"].Merge = true;
						// row
						w.Cells["A4:E4"].Value = $"WP Nr: WP {num_project}"; w.Cells["A4:E4"].Merge = true;
						w.Cells["F4:O4"].Value = ""; w.Cells["F4:O4"].Merge = true;
						w.Cells["P4:Q4"].Value = ""; w.Cells["P4:Q4"].Merge = true;
						// row
						w.Cells["A5:A6"].Value = "Weld Nr."; w.Cells["A5:A6"].Merge = true;
						w.Cells["A5:A6"].Style.TextRotation = 90;
						w.Cells["B5:B6"].Value = "Dimensions, mm"; w.Cells["B5:B6"].Merge = true;
						w.Cells["B5:B6"].Style.TextRotation = 90;

                        w.Cells["C5:C6"].Value = "Length"; w.Cells["C5:C6"].Merge = true;
                        w.Cells["C5:C6"].Style.TextRotation = 90;

						w.Cells["D5:D6"].Value = "Material"; w.Cells["D5:D6"].Merge = true;
						w.Cells["D5:D6"].Style.TextRotation = 90;

						w.Cells["E5:E6"].Value = "Welder Nr."; w.Cells["E5:E6"].Merge = true;
						w.Cells["E5:E6"].Style.TextRotation = 90;
						w.Cells["F5:F6"].Value = "Welder sign"; w.Cells["F5:F6"].Merge = true;
						w.Cells["F5:F6"].Style.TextRotation = 90;
						w.Cells["G5:G6"].Value = "WPS Nr."; w.Cells["G5:G6"].Merge = true;
						w.Cells["G5:G6"].Style.TextRotation = 90;
						w.Cells["H5:H6"].Value = "Welding process"; w.Cells["H5:H6"].Merge = true;
						w.Cells["H5:H6"].Style.TextRotation = 90;
						w.Cells["I5:I6"].Value = "Joint type"; w.Cells["I5:I6"].Merge = true;
						w.Cells["I5:I6"].Style.TextRotation = 90;
						w.Cells["J5:J6"].Value = "Filler material"; w.Cells["J5:J6"].Merge = true;
						w.Cells["J5:J6"].Style.TextRotation = 90;
						w.Cells["K5:K6"].Value = "Preheating temperature, °C"; w.Cells["K5:K6"].Merge = true;
						w.Cells["K5:K6"].Style.TextRotation = 90;
						w.Cells["L5:L6"].Value = "Interpass temperature, °C"; w.Cells["L5:L6"].Merge = true;
						w.Cells["L5:L6"].Style.TextRotation = 90;
						w.Cells["M5:M6"].Value = "PWHT instruction Nr."; w.Cells["M5:M6"].Merge = true;
						w.Cells["M5:M6"].Style.TextRotation = 90;

						w.Cells["N5:N5"].Value = "NDT report Nr."; w.Cells["N5:Q5"].Merge = true;
						w.Cells["N6"].Value = "VT";
						w.Cells["O6"].Value = "RT";
						w.Cells["P6"].Value = "UT";
						w.Cells["Q6"].Value = "PT";
						w.Cells["R6"].Value = "MT";

                        w.Cells["A1:R6"].Style.Font.Bold = true;

                        w.Cells["A1:R6"].Style.Border.Bottom.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells["A1:R6"].Style.Border.Top.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells["A1:R6"].Style.Border.Left.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells["A1:R6"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;

						w.Cells["A1:R6"].Style.HorizontalAlignment = excel.Style.ExcelHorizontalAlignment.Center;
                        w.Cells["A1:R6"].Style.VerticalAlignment = excel.Style.ExcelVerticalAlignment.Center;

                        int p = 6;
						for (int h = 0; h < aw.Count; h++)
                        {
                            p++;
                            ark_weld z = aw[h];
                            w.Cells[$"A{p}"].Value = z.num;
                            w.Cells[$"B{p}"].Value = z.thick1 + "/" + z.thick2 + (" ;a" + z.above);

                            w.Cells[$"C{p}"].Value = z.length.ToString();

                            w.Cells[$"D{p}"].Value = z.material;
                            w.Cells[$"E{p}"].Value = "";
                            w.Cells[$"F{p}"].Value = "";
                            w.Cells[$"H{p}"].Value = "E-ST-07-135";

                            w.Cells[$"K{p}"].Value = "";
                            w.Cells[$"L{p}:R{p}"].Value = "";


                            w.Cells[$"A{p}:R{p}"].Style.Border.Bottom.Style = excel.Style.ExcelBorderStyle.Thin;
							w.Cells[$"A{p}:R{p}"].Style.Border.Top.Style = excel.Style.ExcelBorderStyle.Thin;
							w.Cells[$"A{p}:R{p}"].Style.Border.Left.Style = excel.Style.ExcelBorderStyle.Thin;
							w.Cells[$"A{p}:R{p}"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;

							w.Cells[$"A{p}:R{p}"].Style.HorizontalAlignment = excel.Style.ExcelHorizontalAlignment.Center;
							w.Cells[$"A{p}:R{p}"].Style.VerticalAlignment = excel.Style.ExcelVerticalAlignment.Center;
                        }
                        if (p == 6) p = 7;
                        //Console.WriteLine("ASD " + p.ToString());
                        w.Cells[$"G7:G{p}"].Value = "135";
						w.Cells[$"I7:I{p}"].Value = "ESAB OK AristoRod 12.50; LVS EN ISO 14341-A:  G 42 4 M21 3Si1;";
                        w.Cells[$"I7:I{p}"].Style.WrapText = true;
                        w.Cells[$"I7:I{p}"].Merge = true;
                        w.Cells[$"I7:I{p}"].Style.TextRotation = 90;

						p++;
                        //Console.WriteLine("ASD2 " + p.ToString());
                        w.Cells[$"E{p}"].Value = "QC inspector"; w.Cells[$"E{p}"].Style.Font.Bold = true;
                        w.Cells[$"I{p}"].Value = "Project Manager"; w.Cells[$"I{p}"].Style.Font.Bold = true;
                        w.Cells[$"A{p + 2}"].Value = "Sign and date:";
                        w.Cells[$"E{p + 2}"].Value = "Sign and date:";
                        w.Cells[$"I{p + 2}"].Value = "Sign and date:";
                        //Console.WriteLine("ASD3");
                        w.Cells[$"A{p+2}:R{p+2}"].Style.Border.Bottom.Style = excel.Style.ExcelBorderStyle.Thin;

                        w.Cells[$"D{p}:D{p + 2}"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[$"H{p}:H{p + 2}"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[$"L{p}:L{p + 2}"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[$"R{p}:R{p + 2}"].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;
                        /*w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Bottom.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Top.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Left.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.Border.Right.Style = excel.Style.ExcelBorderStyle.Thin;
						w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.HorizontalAlignment = excel.Style.ExcelHorizontalAlignment.Center;
						w.Cells[letter[0] + "1:" + letter[letter.Length - 1] + (k - 1)].Style.VerticalAlignment = excel.Style.ExcelVerticalAlignment.Center;
*/
                    }
                }
                package.Save();
            }

            MessageBox.Show("Готово");
        }

    }
}


namespace help
{
	public static class x_Profile
	{
		public static x_Profile.Type GetProfileType(Part part)
		{
			string str = "";
			part.GetReportProperty("PROFILE_TYPE", ref str);
			if (str == "I")
				return x_Profile.Type.I;
			if (str == "L")
				return x_Profile.Type.L;
			if (str == "U")
				return x_Profile.Type.U;
			if (str == "T")
				return x_Profile.Type.T;
			if (str == "B")
				return x_Profile.Type.PL;
			if (str == "RO")
				return x_Profile.Type.CHS;
			if (str == "RU")
				return x_Profile.Type.D;
			if (str == "M")
				return x_Profile.Type.RHS;
			if (str == "C")
				return x_Profile.Type.G;
			return str == "Z" ? x_Profile.Type.Z : x_Profile.Type.Unknow;
		}


        public static string GetFlangeAndWeb(Part part, x_Profile.Type profType)
        {
            string result = "-";
            double num = 0.0;
			switch (profType)
			{
				case x_Profile.Type.PL:
                    result = x_Profile.GetPlateThickness(part).ToString();
					break;
				case x_Profile.Type.L:
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS_1", ref num);
					break;
				case x_Profile.Type.I:
				case x_Profile.Type.U:
				case x_Profile.Type.T:
					double val1 = 0.0;
					double val2 = 0.0;
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref val1);
					part.GetReportProperty("PROFILE.WEB_THICKNESS", ref val2);
                    double tnum = Math.Round(Math.Min(val1, val2), 0);
                    if (tnum == 0.0)
                    {
                        part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
                        result = num.ToString();
                        break;
                    }
                    else
                    {
                        result = Math.Round(val1,0) + "(" + Math.Round(val2, 0) + ")";
                        break;
                    }
				case x_Profile.Type.RHS:
				case x_Profile.Type.CHS:
					part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
					break;
                case x_Profile.Type.D:
                    part.GetReportProperty("PROFILE.DIAMETER", ref num); break;
				default:
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref num);
					break;
			}
            if (result == "-") result = Math.Round(num, 0).ToString();
			return result;
        }

		public static double GetThickness(Part part, x_Profile.Type profType)
		{
			double num = 0.0;
			switch (profType)
			{
				case x_Profile.Type.PL:
					num = x_Profile.GetPlateThickness(part);
					break;
				case x_Profile.Type.L:
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS_1", ref num);
					break;
				case x_Profile.Type.I:
				case x_Profile.Type.U:
				case x_Profile.Type.T:
					double val1 = 0.0;
					double val2 = 0.0;
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref val1);
					part.GetReportProperty("PROFILE.WEB_THICKNESS", ref val2);
					num = Math.Min(val1, val2);
					if (num == 0.0)
					{
						part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
						break;
					}
					else
						break;
				case x_Profile.Type.RHS:
				case x_Profile.Type.CHS:
					part.GetReportProperty("PROFILE.PLATE_THICKNESS", ref num);
					break;
				default:
					part.GetReportProperty("PROFILE.FLANGE_THICKNESS", ref num);
					break;
			}
			return num;
		}

		public static double GetPlateThickness(Part part)
		{
			if (part == null)
				return 0.0;
			double num1 = 0.0;
			double num2 = 0.0;
			part.GetReportProperty("PROFILE.HEIGHT", ref num1);
			part.GetReportProperty("PROFILE.WIDTH", ref num2);
			if (num1 < num2)
				return num1;
			else
				return num2;
		}

		public enum Type
		{
			PL,
			L,
			I,
			RHS,
			CHS,
			D,
			U,
			T,
			Z,
			G,
			Unknow,
		}
	}
}