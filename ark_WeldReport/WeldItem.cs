﻿// Type: AutoWelding.WeldItem
// Assembly: ModelWelding, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6CE0C539-AA48-4BBF-A64D-F4872B143C5E
// Assembly location: C:\Users\Sergey_2\AppData\Roaming\Skype\My Skype Received Files\ModelWelding.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tekla.Structures.Geometry3d;
using Tekla.Structures.Model;
using Tekla.Structures.Model.UI;
using Tekla.Structures.Solid;

namespace AutoWelding
{
  internal class WeldItem
  {
    public ModelObject Object1 { get; set; }

    public ModelObject Object2 { get; set; }

    public Edge Edge { get; set; }

    public Face Face { get; set; }

    public LineSegment ProjectedLine { get; set; }

    public List<LineSegment> WeldLines { get; set; }

    public double Size { get; set; }

    public double MaxGap { get; set; }

    public double Tolerance { get; set; }

    public bool IsOnOtherSide { get; set; }

    public WeldItem(ModelObject object1, ModelObject object2, Edge edge, Face face, LineSegment projectedLine, double maxGap, double tolerance, bool isKatetByValue, double katetValue)
    {
      this.Object1 = object1;
      this.Object2 = object2;
      this.Edge = edge;
      this.Face = face;
      this.ProjectedLine = projectedLine;
      this.MaxGap = maxGap;
      this.Tolerance = tolerance;
            this.Size = Math.Min(this.GetWeldSize()[0], this.GetWeldSize()[1]);
      if (isKatetByValue && this.Size > 0.0)
        this.Size = katetValue;
      this.WeldLines = this.GetWeldLines();
    }

    private List<LineSegment> GetWeldLines()
    {
      List<LineSegment> list1 = new List<LineSegment>();
      try
      {
        bool flag1 = false;
        bool flag2 = false;
        List<LineSegment> loopEdges = this.GetLoopEdges(this.Face);
        Part part1 = this.Object1 as Part;
        Part part2 = this.Object2 as Part;
        if (this.Object1.Equals(this.Object2))
          return list1;
        double num1 = this.ProjectedLine.Length() * 50.0;
        ArrayList Points = new ArrayList();
        LoopEnumerator loopEnumerator = this.Face.GetLoopEnumerator();
        while (loopEnumerator.MoveNext())
        {
					VertexEnumerator vertexEnumerator = (loopEnumerator.Current as Loop).GetVertexEnumerator();
          while (vertexEnumerator.MoveNext())
            Points.Add((object) vertexEnumerator.Current);
        }
        if (WeldItem.Polygon3dArea(Points) > num1)
        {
          Solid solid = part1.GetSolid();
          if (solid != null)
          {
            EdgeEnumerator edgeEnumerator = solid.GetEdgeEnumerator();
            while (edgeEnumerator.MoveNext())
            {
              Edge edge = edgeEnumerator.Current as Edge;
              if (edge != null && (Distance.PointToPoint(this.ProjectedLine.Point1, edge.StartPoint) < this.MaxGap && Distance.PointToPoint(this.ProjectedLine.Point2, edge.EndPoint) < this.MaxGap || Distance.PointToPoint(this.ProjectedLine.Point1, edge.EndPoint) < this.MaxGap && Distance.PointToPoint(this.ProjectedLine.Point2, edge.StartPoint) < this.MaxGap))
              {
                list1.Add(new LineSegment(this.ProjectedLine.Point1, this.ProjectedLine.Point2));
                List<PolygonWeld> list2 = new List<PolygonWeld>();
                ModelObjectEnumerator welds1 = part1.GetWelds();
                while (welds1.MoveNext())
                {
                  PolygonWeld polygonWeld = welds1.Current as PolygonWeld;
                  if (polygonWeld != null)
                    list2.Add(polygonWeld);
                }
                ModelObjectEnumerator welds2 = part2.GetWelds();
                while (welds2.MoveNext())
                {
                  PolygonWeld polygonWeld = welds1.Current as PolygonWeld;
                  if (polygonWeld != null)
                    list2.Add(polygonWeld);
                }
                foreach (PolygonWeld polygonWeld in list2)
                {
                  List<Point> list3 = new List<Point>();
                  foreach (object obj in polygonWeld.Polygon.Points)
                    list3.Add(obj as Point);
                  foreach (LineSegment lineSegment in list1)
                  {
                    if (Distance.PointToPoint(list3[0], lineSegment.Point1) < this.Tolerance * 2.0 && Distance.PointToPoint(list3[1], lineSegment.Point2) < this.Tolerance * 2.0 || Distance.PointToPoint(list3[1], lineSegment.Point1) < this.Tolerance * 2.0 && Distance.PointToPoint(list3[0], lineSegment.Point2) < this.Tolerance * 2.0)
                    {
                      list1.Clear();
                      return list1;
                    }
                  }
                }
              }
            }
          }
        }
        if (this.IsOnOtherSide)
          return list1;
        foreach (LineSegment LineSegment1 in loopEdges)
        {
          if (Parallel.LineSegmentToLineSegment(LineSegment1, this.ProjectedLine) && Distance.PointToLine(this.ProjectedLine.Point1, new Line(LineSegment1.Point1, LineSegment1.Point2)) < this.Tolerance * 2.0)
            return list1;
        }
        Model model = new Model();
        TransformationPlane transformationPlane = model.GetWorkPlaneHandler().GetCurrentTransformationPlane();
        Vector normal = this.Face.Normal;
        Vector vector1 = new Vector(this.Edge.EndPoint.X - this.Edge.StartPoint.X, this.Edge.EndPoint.Y - this.Edge.StartPoint.Y, this.Edge.EndPoint.Z - this.Edge.StartPoint.Z);
        TransformationPlane TransformationPlane = new TransformationPlane(this.Edge.StartPoint, vector1.GetNormal(), normal.GetNormal());
        model.GetWorkPlaneHandler().SetCurrentTransformationPlane(TransformationPlane);
        MatrixFactory.ByCoordinateSystems(new CoordinateSystem(new Point(), new Vector(1.0, 0.0, 0.0), new Vector(0.0, 1.0, 0.0)), new CoordinateSystem(this.Edge.StartPoint, vector1.GetNormal(), normal.GetNormal()));
        GraphicsDrawer graphicsDrawer = new GraphicsDrawer();
        List<double> list4 = new List<double>();
        List<double> list5 = new List<double>();
        foreach (LineSegment lineSegment in loopEdges)
        {
          list4.Add(lineSegment.Point1.X);
          list4.Add(lineSegment.Point2.X);
          list5.Add(lineSegment.Point1.Y);
          list5.Add(lineSegment.Point2.Y);
        }
        double num2 = Enumerable.Min((IEnumerable<double>) list4) - this.Tolerance;
        double num3 = Enumerable.Max((IEnumerable<double>) list4) + this.Tolerance;
        double num4 = Enumerable.Min((IEnumerable<double>) list5) - this.Tolerance;
        double num5 = Enumerable.Max((IEnumerable<double>) list5) + this.Tolerance;
        Vector vector2 = new Vector(this.ProjectedLine.Point2.X - this.ProjectedLine.Point1.X, this.ProjectedLine.Point2.Y - this.ProjectedLine.Point1.Y, this.ProjectedLine.Point2.Z - this.ProjectedLine.Point1.Z).GetNormal() * this.Tolerance;
        if (this.ProjectedLine.Point1.X < num2 || this.ProjectedLine.Point1.X > num3 || this.ProjectedLine.Point1.Y < num4 || this.ProjectedLine.Point1.Y > num5)
        {
          flag1 = false;
        }
        else
        {
          int num6 = 0;
          Point Point1 = new Point();
          foreach (LineSegment LineSegment in loopEdges)
          {
            if (Distance.PointToPoint(Point1, LineSegment.Point1) > this.Tolerance && Distance.PointToPoint(Point1, LineSegment.Point2) > this.Tolerance)
            {
              double num7 = Distance.PointToLineSegment(this.ProjectedLine.Point1, LineSegment);
              if (num7 > -this.Tolerance && num7 < this.Tolerance)
              {
                flag1 = true;
                break;
              }
              else
              {
                Point point1 = this.ProjectedLine.Point1;
                while (Distance.PointToPoint(this.ProjectedLine.Point1, point1) < 10000.0)
                {
                  if (Distance.PointToLineSegment(point1, LineSegment) < this.Tolerance)
                  {
                    if (Distance.PointToPoint(point1, LineSegment.Point1) < this.Tolerance || Distance.PointToPoint(point1, LineSegment.Point2) < this.Tolerance)
                      Point1 = point1;
                    ++num6;
                    break;
                  }
                  else
                    point1 += (Point) vector2;
                }
              }
            }
          }
          if (WeldItem.IsOdd(num6))
            flag1 = true;
        }
        Vector vector3 = new Vector(this.ProjectedLine.Point1.X - this.ProjectedLine.Point2.X, this.ProjectedLine.Point1.Y - this.ProjectedLine.Point2.Y, this.ProjectedLine.Point1.Z - this.ProjectedLine.Point2.Z).GetNormal() * this.Tolerance;
        if (this.ProjectedLine.Point2.X < num2 || this.ProjectedLine.Point2.X > num3 || this.ProjectedLine.Point2.Y < num4 || this.ProjectedLine.Point2.Y > num5)
        {
          flag2 = false;
        }
        else
        {
          int num6 = 0;
          Point Point1 = new Point();
          foreach (LineSegment LineSegment in loopEdges)
          {
            if (Distance.PointToPoint(Point1, LineSegment.Point1) > this.Tolerance && Distance.PointToPoint(Point1, LineSegment.Point2) > this.Tolerance)
            {
              double num7 = Distance.PointToLineSegment(this.ProjectedLine.Point2, LineSegment);
              if (num7 > -this.Tolerance && num7 < this.Tolerance)
              {
                flag2 = true;
                break;
              }
              else
              {
                Point point2 = this.ProjectedLine.Point2;
                while (Distance.PointToPoint(this.ProjectedLine.Point2, point2) < 10000.0)
                {
                  if (Distance.PointToLineSegment(point2, LineSegment) < this.Tolerance)
                  {
                    if (Distance.PointToPoint(point2, LineSegment.Point1) < this.Tolerance || Distance.PointToPoint(point2, LineSegment.Point2) < this.Tolerance)
                      Point1 = point2;
                    ++num6;
                    break;
                  }
                  else
                    point2 += (Point) vector3;
                }
              }
            }
          }
          if (WeldItem.IsOdd(num6))
            flag2 = true;
        }
        List<Point> list6 = new List<Point>();
        if (flag1)
          list6.Add(this.ProjectedLine.Point1);
        foreach (LineSegment LineSegment in loopEdges)
        {
          Point point1 = this.ProjectedLine.Point1;
          while (Distance.PointToPoint(point1, this.ProjectedLine.Point2) > this.Tolerance)
          {
            double num6 = Distance.PointToLineSegment(point1, LineSegment);
            if (num6 <= 10000.0)
            {
              if (num6 < this.Tolerance && !list6.Contains(point1))
              {
                list6.Add(point1);
                break;
              }
              else
                point1 += (Point) vector2;
            }
            else
              break;
          }
        }
        if (flag2)
          list6.Add(this.ProjectedLine.Point2);
        if (list6.Count > 1 && Distance.PointToPoint(list6[0], list6[1]) < this.Tolerance * 2.0)
          list6.Remove(list6[1]);
        if (list6.Count > 1 && Distance.PointToPoint(list6[list6.Count - 2], list6[list6.Count - 1]) < this.Tolerance * 2.0)
          list6.Remove(list6[list6.Count - 2]);
        if (!WeldItem.IsOdd(list6.Count))
        {
          int index = 0;
          while (index < list6.Count)
          {
            list1.Add(new LineSegment(list6[index], list6[index + 1]));
            index += 2;
          }
        }
        model.GetWorkPlaneHandler().SetCurrentTransformationPlane(transformationPlane);
        List<PolygonWeld> list7 = new List<PolygonWeld>();
        ModelObjectEnumerator welds3 = part1.GetWelds();
        while (welds3.MoveNext())
        {
          PolygonWeld polygonWeld = welds3.Current as PolygonWeld;
          if (polygonWeld != null)
            list7.Add(polygonWeld);
        }
        ModelObjectEnumerator welds4 = part2.GetWelds();
        while (welds4.MoveNext())
        {
          PolygonWeld polygonWeld = welds3.Current as PolygonWeld;
          if (polygonWeld != null)
            list7.Add(polygonWeld);
        }
        foreach (PolygonWeld polygonWeld in list7)
        {
          List<Point> list2 = new List<Point>();
          foreach (object obj in polygonWeld.Polygon.Points)
            list2.Add(obj as Point);
          foreach (LineSegment lineSegment in list1)
          {
            if (Distance.PointToPoint(list2[0], lineSegment.Point1) < this.Tolerance * 2.0 && Distance.PointToPoint(list2[1], lineSegment.Point2) < this.Tolerance * 2.0 || Distance.PointToPoint(list2[1], lineSegment.Point1) < this.Tolerance * 2.0 && Distance.PointToPoint(list2[0], lineSegment.Point2) < this.Tolerance * 2.0)
            {
              list1.Clear();
              return list1;
            }
          }
        }
      }
      catch (Exception ex)
      {
        //Utils.CatchException(ex);
      }
      return list1;
    }
        public List<double> GetWeldSize()
    {
      double val1 = 0.0;
      double val2 = 0.0;
      try
      {
        Solid solid1 = (this.Object1 as Part).GetSolid();
        if (solid1 != null)
        {
          double num1 = 1000.0;
          FaceEnumerator faceEnumerator = solid1.GetFaceEnumerator();
          while (faceEnumerator.MoveNext())
          {
            Face current = faceEnumerator.Current as Face;
            if (current != null)
            {
              Point point = new Point();
              LoopEnumerator loopEnumerator1 = this.Face.GetLoopEnumerator();
              int num2 = 0;
              int num3 = 0;
              while (loopEnumerator1.MoveNext())
              {
                if (num2 <= 1)
                {
									VertexEnumerator vertexEnumerator = (loopEnumerator1.Current as Loop).GetVertexEnumerator();
                  while (vertexEnumerator.MoveNext())
                  {
                    if (num3 <= 1)
                      point = vertexEnumerator.Current as Point;
                    ++num3;
                  }
                }
                ++num2;
              }
              Vector normal1 = this.Face.Normal;
              Vector vector1 = new Vector(this.Edge.EndPoint.X - this.Edge.StartPoint.X, this.Edge.EndPoint.Y - this.Edge.StartPoint.Y, this.Edge.EndPoint.Z - this.Edge.StartPoint.Z);
              GeometricPlane Plane1 = new GeometricPlane(point, normal1);
              Point Origin = new Point();
              LoopEnumerator loopEnumerator2 = current.GetLoopEnumerator();
              int num4 = 0;
              int num5 = 0;
              while (loopEnumerator2.MoveNext())
              {
                if (num4 <= 1)
                {
									VertexEnumerator vertexEnumerator = (loopEnumerator2.Current as Loop).GetVertexEnumerator();
                  while (vertexEnumerator.MoveNext())
                  {
                    if (num5 <= 1)
                      Origin = vertexEnumerator.Current as Point;
                    ++num5;
                  }
                }
                ++num4;
              }
              Vector normal2 = current.Normal;
              Vector vector2 = new Vector(this.Edge.EndPoint.X - this.Edge.StartPoint.X, this.Edge.EndPoint.Y - this.Edge.StartPoint.Y, this.Edge.EndPoint.Z - this.Edge.StartPoint.Z);
              GeometricPlane geometricPlane = new GeometricPlane(Origin, normal2);
              double num6 = Distance.PointToPlane(point, geometricPlane);
              if (num6 > this.Tolerance && Parallel.PlaneToPlane(Plane1, geometricPlane) && num6 < num1)
              {
                val1 = num6;
                num1 = num6;
              }
            }
          }
        }
        Solid solid2 = (this.Object2 as Part).GetSolid(Solid.SolidCreationTypeEnum.NORMAL_WITHOUT_EDGECHAMFERS);
        Edge closestEdgeOnChamfer = this.GetClosestEdgeOnChamfer(this.Edge);
        if (solid2 != null)
        {
          double num1 = 1000.0;
          List<Face> list1 = new List<Face>();
          FaceEnumerator faceEnumerator1 = solid2.GetFaceEnumerator();
          while (faceEnumerator1.MoveNext())
          {
            List<Point> list2 = new List<Point>();
            Face current = faceEnumerator1.Current as Face;
            if (current != null)
            {
              LoopEnumerator loopEnumerator = current.GetLoopEnumerator();
              while (loopEnumerator.MoveNext())
              {
								VertexEnumerator vertexEnumerator = (loopEnumerator.Current as Loop).GetVertexEnumerator();
                while (vertexEnumerator.MoveNext())
									list2.Add(vertexEnumerator.Current as  Point);
              }
            }
            bool flag1 = false;
            bool flag2 = false;
            foreach (Point Point2 in list2)
            {
              if (Distance.PointToPoint(closestEdgeOnChamfer.StartPoint, Point2) < this.Tolerance)
                flag1 = true;
              if (Distance.PointToPoint(closestEdgeOnChamfer.EndPoint, Point2) < this.Tolerance)
                flag2 = true;
            }
            if (flag1 & flag2)
              list1.Add(current);
          }
          FaceEnumerator faceEnumerator2 = solid2.GetFaceEnumerator();
          while (faceEnumerator2.MoveNext())
          {
            Face current = faceEnumerator2.Current as Face;
            if (current != null)
            {
              foreach (Face face in list1)
              {
                Point Origin1 = new Point();
                LoopEnumerator loopEnumerator1 = face.GetLoopEnumerator();
                int num2 = 0;
                int num3 = 0;
                while (loopEnumerator1.MoveNext())
                {
                  if (num2 <= 1)
                  {
										VertexEnumerator vertexEnumerator = (loopEnumerator1.Current as Loop).GetVertexEnumerator();
                    while (vertexEnumerator.MoveNext())
                    {
                      if (num3 <= 1)
                        Origin1 = vertexEnumerator.Current as Point;
                      ++num3;
                    }
                  }
                  ++num2;
                }
                Vector normal1 = face.Normal;
                GeometricPlane geometricPlane = new GeometricPlane(Origin1, normal1);
                Point Origin2 = new Point();
                LoopEnumerator loopEnumerator2 = current.GetLoopEnumerator();
                int num4 = 0;
                int num5 = 0;
                while (loopEnumerator2.MoveNext())
                {
                  if (num4 <= 1)
                  {
										VertexEnumerator vertexEnumerator = (loopEnumerator2.Current as Loop).GetVertexEnumerator();
                    while (vertexEnumerator.MoveNext())
                    {
                      if (num5 <= 1)
												Origin2 = vertexEnumerator.Current as Point;
                      ++num5;
                    }
                  }
                  ++num4;
                }
                Vector normal2 = current.Normal;
                GeometricPlane Plane = new GeometricPlane(Origin2, normal2);
                double angleBetween = normal1.GetAngleBetween(normal2);
                if (angleBetween > WeldItem.DegToRad(175.0) && angleBetween < WeldItem.DegToRad(185.0))
                {
                  Vector vector = new Vector(closestEdgeOnChamfer.EndPoint.X - closestEdgeOnChamfer.StartPoint.X, closestEdgeOnChamfer.EndPoint.Y - closestEdgeOnChamfer.StartPoint.Y, closestEdgeOnChamfer.EndPoint.Z - closestEdgeOnChamfer.StartPoint.Z);
                  double num6 = Distance.PointToPlane(new Point((closestEdgeOnChamfer.EndPoint.X + closestEdgeOnChamfer.StartPoint.X) / 2.0, (closestEdgeOnChamfer.EndPoint.Y + closestEdgeOnChamfer.StartPoint.Y) / 2.0, (closestEdgeOnChamfer.EndPoint.Z + closestEdgeOnChamfer.StartPoint.Z) / 2.0), Plane);
                  if (num6 > this.Tolerance && num6 < num1)
                  {
                    val2 = num6;
                    num1 = num6;
                  }
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        //Utils.CatchException(ex);
      }
            //return Math.Round(Math.Min(val1, val2), 0, MidpointRounding.AwayFromZero);
            return new List<double>() { Math.Round(val1, 0), Math.Round(val2, 0) };
    }

    private Edge GetClosestEdgeOnChamfer(Edge edge)
    {
      try
      {
        List<Edge> list1 = new List<Edge>();
        Solid solid1 = (this.Object2 as Part).GetSolid();
        if (solid1 != null)
        {
          EdgeEnumerator edgeEnumerator = solid1.GetEdgeEnumerator();
          while (edgeEnumerator.MoveNext())
            list1.Add(edgeEnumerator.Current as Edge);
        }
        List<Edge> list2 = new List<Edge>();
        List<Edge> list3 = new List<Edge>();
        Solid solid2 = (this.Object2 as Part).GetSolid(Solid.SolidCreationTypeEnum.NORMAL_WITHOUT_EDGECHAMFERS);
        if (solid2 != null)
        {
          EdgeEnumerator edgeEnumerator = solid2.GetEdgeEnumerator();
          while (edgeEnumerator.MoveNext())
          {
            Edge edge1 = edgeEnumerator.Current as Edge;
            list2.Add(edge1);
            bool flag = false;
            foreach (Edge edge2 in list1)
            {
              if (Distance.PointToPoint(edge1.StartPoint, edge2.StartPoint) < this.Tolerance && Distance.PointToPoint(edge1.EndPoint, edge2.EndPoint) < this.Tolerance || Distance.PointToPoint(edge1.StartPoint, edge2.EndPoint) < this.Tolerance && Distance.PointToPoint(edge1.EndPoint, edge2.StartPoint) < this.Tolerance)
                flag = true;
            }
            if (!flag)
              list3.Add(edge1);
          }
        }
        if (list1.Count == list2.Count)
          return edge;
        Dictionary<double, Edge> dictionary = new Dictionary<double, Edge>();
        bool flag1 = true;
        foreach (Edge edge1 in list3)
        {
          if (Parallel.LineSegmentToLineSegment(new LineSegment(edge.StartPoint, edge.EndPoint), new LineSegment(edge1.StartPoint, edge1.EndPoint)) && Distance.PointToLineSegment(edge.StartPoint, new LineSegment(edge1.StartPoint, edge1.EndPoint)) < 100.0)
          {
            double key = Distance.PointToLineSegment(edge.StartPoint, new LineSegment(edge1.StartPoint, edge1.EndPoint));
            dictionary.Add(key, edge1);
            flag1 = false;
          }
        }
        if (flag1 || Enumerable.Count<KeyValuePair<double, Edge>>((IEnumerable<KeyValuePair<double, Edge>>) dictionary) <= 0)
          return edge;
        else
          return dictionary[Enumerable.Min((IEnumerable<double>) dictionary.Keys)];
      }
      catch (Exception ex)
      {
        //Utils.CatchException(ex);
      }
      return edge;
    }

    private List<LineSegment> GetLoopEdges(Face face)
    {
      List<LineSegment> list = new List<LineSegment>();
      try
      {
        Point Point2 = new Point();
        LoopEnumerator loopEnumerator = face.GetLoopEnumerator();
        Point Point1 = new Point();
        while (loopEnumerator.MoveNext())
        {
					VertexEnumerator vertexEnumerator = (loopEnumerator.Current as Loop).GetVertexEnumerator();
          while (vertexEnumerator.MoveNext())
          {
            if (Point1.X == 0.0 && Point1.Y == 0.0 && Point1.Z == 0.0)
            {
              Point2 = vertexEnumerator.Current as Point;
              Point1 = vertexEnumerator.Current as Point;
            }
            else
            {
              list.Add(new LineSegment(Point1, vertexEnumerator.Current as Point));
              Point1 = vertexEnumerator.Current as Point;
            }
          }
          list.Add(new LineSegment(Point1, Point2));
        }
      }
      catch (Exception ex)
      {
        //Utils.CatchException(ex);
      }
      return list;
    }

    private static bool IsOdd(int value)
    {
      return (uint) (value % 2) > 0U;
    }

    private static double RadToDeg(double rad)
    {
      return rad * 180.0 / Math.PI;
    }

    private static double DegToRad(double deg)
    {
      return deg * Math.PI / 180.0;
    }

    public static double Polygon3dArea(ArrayList Points)
    {
      Point point1 = Points[0] as Point;
      Point point2 = Points[1] as Point;
      Point point3 = Points[2] as Point;
      Vector vector1 = new Vector();
      Vector vector2 = new Vector();
      vector1.X = point1.X - point2.X;
      vector1.Y = point1.Y - point2.Y;
      vector1.Z = point1.Z - point2.Z;
      vector2.X = point2.X - point3.X;
      vector2.Y = point2.Y - point3.Y;
      vector2.Z = point2.Z - point3.Z;
      Vector vector3 = new Vector();
      vector3.X = vector1.Y * vector2.Z - vector1.Z * vector2.Y;
      vector3.Y = vector1.Z * vector2.X - vector1.X * vector2.Z;
      vector3.Z = vector1.X * vector2.Y - vector1.Y * vector2.X;
      int count = Points.Count;
      Points.Add((object) point1);
      Points.Add((object) point2);
      double num1 = 0.0;
      double num2 = vector3.X > 0.0 ? vector3.X : -vector3.X;
      double num3 = vector3.Y > 0.0 ? vector3.Y : -vector3.Y;
      double num4 = vector3.Z > 0.0 ? vector3.Z : -vector3.Z;
      int num5 = 3;
      if (num2 > num3)
      {
        if (num2 > num4)
          num5 = 1;
      }
      else if (num3 > num4)
        num5 = 2;
      int index1 = 1;
      int index2 = 2;
      int index3 = 0;
      while (index1 <= count)
      {
        Point point4 = Points[index1] as Point;
        Point point5 = Points[index2] as Point;
        Point point6 = Points[index3] as Point;
        switch (num5)
        {
          case 1:
            num1 += point4.Y * (point5.Z - point6.Z);
            break;
          case 2:
            num1 += point4.X * (point5.Z - point6.Z);
            break;
          case 3:
            num1 += point4.X * (point5.Y - point6.Y);
            break;
        }
        ++index1;
        ++index2;
        ++index3;
      }
      double num6 = Math.Sqrt(num2 * num2 + num3 * num3 + num4 * num4);
      switch (num5)
      {
        case 1:
          num1 *= num6 / (2.0 * num2);
          break;
        case 2:
          num1 *= num6 / (2.0 * num3);
          break;
        case 3:
          num1 *= num6 / (2.0 * num4);
          break;
      }
      return Math.Abs(num1);
    }
  }
}
