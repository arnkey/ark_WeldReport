﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ark_WeldReport
{
	static class Program
	{
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main()
		{
            if (Protect())
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
		}

        static bool Protect()
        {
            DateTime dt = DateTime.Parse("06.09.2017");
            if (DateTime.Now <= dt) return true;
            // return false;
            return true;
        }
	}
}
